<?php
 
function GetUpdateObject($row){
    if ($row['type'] == 1) //move
    {
        return new MoveUpdate($row["gameid"],$row["id"],$row["param1"],$row["param2"],$row["param3"]);
    } elseif ($row["type"]==2) //attack
    {
        return new AttackUpdate($row["gameid"],$row["id"],$row["param1"],$row["param2"],$row["param3"]);
    } else if ($row["type"]==3) //turn update
    {
        return new TurnUpdate($row["gameid"],(int)$row["id"],(int)$row["param1"]);
    }else if ($row["type"]==4) //spawn update
    {
        return new SpawnUnitUpdate($row["gameid"],(int)$row["id"],(int)$row["param1"]);
    }
}

class MoveUpdate
{
    public $GameID;
    public $UpdateID;
    public $TypeName = "Move";
    //public $Type = 1;
    public $UnitID;
    public $X;
    public $Y;
    
    public function __construct ($GameID, $UpdateID, $UnitID, $X, $Y)
    {
        $this->GameID=$GameID;
        $this->UpdateID = $UpdateID;
        $this->UnitID=$UnitID;
        $this->X=$X;
        $this->Y=$Y;
        
    }
}

class AttackUpdate
{
    public $GameID;
    public $UpdateID;
    public $TypeName = "Attack";
    //public $Type = 2;
    public $AttackerID;
    public $DefenderID;
    public $Damage;
    
    public function __construct ($GameID, $UpdateID, $AttackerID, $DefenderID, $Damage){
        $this->GameID=$GameID;
        $this->UpdateID=$UpdateID;
        $this->AttackerID=$AttackerID;
        $this->DefenderID=$DefenderID;
        $this->Damage=$Damage;
    }
}

class SpawnUnitUpdate
{
    public $GameID;
    public $UpdateID;
    public $TypeName = "SpawnUnit";
    //public $Type = 2;
    public $UnitID;
    
    public function __construct ($GameID, $UpdateID, $UnitID){
        $this->GameID=$GameID;
        $this->UpdateID=$UpdateID;
        $this->UnitID=$UnitID;
    }
}

class TurnUpdate
{
    public $GameID;
    public $UpdateID;
    public $TypeName = "BeginTurn";
    //public $Type = 3;
    public $NewPlayerID;

    public function __construct ($GameID, $UpdateID, $NewPlayerID){
        $this->GameID=$GameID;
        $this->UpdateID=$UpdateID;
        $this->NewPlayerID=$NewPlayerID;
    }
    
}

?>
