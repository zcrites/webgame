<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include('dbconns.php');
include_once('primitives.php');
include('updatelib.php');

class GameDataService
{
    
    public static function GetInstance()
    {
       if( !self::$instance)
           self::$instance = new GameDataService();
       return self::$instance;
    }
    
    public static function GetGame( $id )
    {
        $i = self::GetInstance();
        return $i->GetGameByID( $id );
    }
    
    public function GetUserById( $userid )
    {
        return $this->getUserData( "id = $userid");
    }
    
    public function GetUserByName( $username )
    {
        return $this->getUserData( "user = '$username'");
    }
    
    public function GetGameByID( $gameID )
    {
        $query = "SELECT * FROM games WHERE id=$gameID";
        $result = mysql_query( $query ) or die(mysql_error());
        $row = mysql_fetch_array($result);
        
        $g = new GameData();
        
        $g->ID = (int)$gameID;
        $g->Label = $row['label'];
        $g->ActiveUser = (int)$row['activeuser'];
        $g->Width = (int)$row['width'];
        $g->Height = (int)$row['height'];
        $g->Seed = (int)$row['seed'];
        $g->UpdateID = (int)$row['updateid'];    
        $g->Turn = (int)$row['turn'];
        
        $this->initGamePlayers( $g );
        $this->initGameUnits( $g );
        return $g;
    }
    
    public function GetGameMap( $gamedata )
    {  
        return $this->createMapFromGameData( $gamedata );
    }
   
    //private

    private static $instance;
    
    private function __construct()
    {
        MySql::Init();
    }

    private function getUserData( $where )
    {
        $u = new UserData();
        
        $query = "SELECT * FROM userpass WHERE $where";       
        $result = mysql_query( $query ) or die(mysql_error());
        $row = mysql_fetch_array($result);
        
        if(!$row ) return null;
        $u->ID = (int)$row['id'];
        $u->UserName = $row['user'];
        $u->Wins = (int)$row['wins'];
        $u->Color = $row['color'];
        return $u;
    }
    
    private function initGamePlayers( &$gamedata )
    {
        $query = "SELECT * FROM userpass, usergames WHERE usergames.gameid = $gamedata->ID AND userpass.id = usergames.userid";
        $result = mysql_query( $query ) or die(mysql_error());        
        $players = array();
        while( $row = mysql_fetch_array($result))
        {
            $p = $this->createPlayerDataFromRow( $row );
            if( $gamedata->ActiveUser == $p->UserID)
                $gamedata->ActivePlayer = $p;
            $players[] = $p;
        }    
        $gamedata->Players = $players;
    }
    
    private function createPlayerDataFromRow( $row )
    {
        $p = new PlayerData();
        
        $p->GameID = (int)$row['gameid'];
        $p->UserID = (int)$row['id'];
        $p->UserName = (string)$row['user'];
        $p->Color = (string)$row['color'];        
        $p->Resources = (int)$row['resources'];
        
        return $p;
    }
    

    public static function generateUnitQueryWhere( $whereClause )
    {
        $unitquery = "SELECT units.*, 
                    userpass.color, userpass.user
                    FROM units, userpass WHERE userpass.id = units.ownerid";
        
        return $unitquery." AND ( ".$whereClause." )";
    }
    
    private function initGameUnits( &$gamedata )
    {
        $query = $this->generateUnitQueryWhere("units.gameid = $gamedata->ID");
        $result = mysql_query( $query ) or die(mysql_error());        
        $units = array();
        while( $row = mysql_fetch_array($result))
        {
            $units[] = $this->createUnitDataFromRow( $row );
        }    
        return $gamedata->Units = $units;
    }
    
    private function createUnitDataFromRow( $row )
    {
        $u = new UnitData();
        ///die((string))."<br/>");
        $u->ID = (int)$row['id'];
        $u->OwnerID = (int)$row['ownerid'];
        $u->OwnerName = (string)$row['user'];
        $u->Name = (string)$row['name'];
        $u->Type = (int)$row['type'];
        $u->X = (int)$row['x'];
        $u->Y = (int)$row['y'];
        $u->Color = $row['color'];
        $u->HP = (int)$row['hp'];
        $u->Moves = (int)$row['moves'];
        $u->ActionUsed = (bool)$row['actionused'];
        
        $t = UnitConfiguration::GetByID( $u->Type );
        
        //$this->Stats = new UnitStats($row);
        $u->MaxHP = $t->MaxHP;
        $u->AttackPower = $t->AttackPower;
        $u->Defense = $t->Defense;
        $u->AttackRange = $t->AttackRange;
        $u->MoveRange = $t->MoveRange;
        $u->TypeName = $t->Name;
        $u->GatherSpeed = $t->GatherSpeed;
        $u->GameID = (int)$row['gameid'];
        
        return $u;
    }    
    
    private function createMapFromGameData( $gamedata )
    {
        $m = new Map();
        $m->Width = $gamedata->Width;
        $m->Height = $gamedata->Height;
        srand( $gamedata->Seed );
        
        for( $i = 0; $i < $gamedata->Width; $i++)                
        {
            $m->Cells[$i] = array();
            for( $j = 0; $j < $gamedata->Height; $j++)        
            {
                $v = rand(0,99);
                $t = 3;
                $t = ( $v < 100 ) ? 2 : $t;
                $t = ( $v < 99 ) ? 1 : $t;
                $t = ( $v < 79 ) ? 0 : $t;

                $m->Cells[$i][$j] = new Cell( $i, $j, $t );

            }
        }
        $this->genMapWater($m);
        
        srand();
        return $m;
    }
    
    private function genMapWater( &$map )
    {
        $x = 0;
        $y = rand(1,$map->Height-2);
     
        while( $x < $map->Width-1 && $y > 0 && $y < $map->Height-2)
        {
            if( $x % 2 == 1 )
                $y += 1;
            $y -= rand(0,1);
            if( rand(0,2) != 0)
                $map->Cells[$x][$y] = new Cell( $x, $y, 3 );
            if( rand(0,2) != 0)
                $map->Cells[$x][$y+1] = new Cell( $x, $y, 3 );
            $x ++;
        }        
    }   
}

?>
