<?php

function my_error_handler( $errno , $errstr, $errfile, $errline){
    die(json_encode( array( 'status' => $errstr . ' in file ' . $errfile . ' line ' . $errline) ));
    return FALSE;
}

set_error_handler('my_error_handler');

// database objects

class UserData
{
    public $ID;
    public $UserName; 
}

class PlayerData 
{
    public $GameID;
    public $UserID;
    public $UserName;
    public $Color;
    public $Resources;
}

class GameData
{
    public $ID;
    public $Label;
    public $ActiveUser;
    public $Width;
    public $Height;
    public $Seed;
    public $UpdateID;
    
    public $ActivePlayer = null;
    
    public $Units = null;
    public $Players = null;
    
}

class UnitData
{
    
    public $ID;
    public $OwnerID;
    public $OwnerName;
    public $Name;
    public $Type;
    public $X;
    public $Y;
    public $HP;
    public $ActionUsed;
    public $Color;
    public $Moves;
    public $GameID;
        
    public $MaxHP;
    public $AttackPower;
    public $Defense;
    public $AttackRange;
    public $MoveRange;
    public $TypeName;
      
}

class Cell
{
    private $X;
    private $Y;
    public $Type;
    
    public function Cell( $x, $y, $type )
    {
        $this->X = $x;
        $this->Y = $y;
        $this->Type = $type;
    }
}


class Map
{
    public $Width;
    public $Height;
    public $Seed;
    
    public $Cells = array();
}


// php config objects

?>
