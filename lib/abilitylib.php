<?php

include('validation.php');

    /*Ability apply functions. These get called when $ability->Apply() is called
    Each one has identical arguments:
        $ability is the ability that's being used. It has Params, which is some array of the ability parameters
        $game is a gamedata object, or something
        $unitrow is an array of unit data (retrived by mysql_fetch_array off of a mysql_query)
        $newParams is whatever the Apply function needs to make the ability happend (e.g. for moving, it should have new coords)
        $updateList should be modified to include whatever new updates need to be stored when the ability is used
    */
    function lifeauraApply($ability, $game, $unitrow, $newParams, &$updateList)
    {
        $range = $ability->Params[0];
        $power = $ability->Params[1];
        if ($power >= 0){ // healing aura on own units
            $newUnits = GetMyUnits($unitrow['ownerid'], $game->ID);
        } else { // damage aura on enemies
            $newUnits = GetTheirUnits($unitrow['ownerid'], $game->ID);
        }
        $affected = AllCellsInRange($game,$unitrow['x'],$unitrow['y'],$range,TRUE);
        while ($row = mysql_fetch_array($newUnits)){
            if ( isset( $affected[$row['x']][$row['y']] ) ){
                DamageApply(-1*$power, 0, $row['hp'], $unitrow['id'], $row['id'], $row['maxhp'], $game->ID, $updateList);
            }
        }
        return;
    }
    
    function moveApply($ability, $game, $unitrow, $newParams, &$updateList)
    {
        $x = $newParams['x'];
        $y = $newParams['y'];
        $unitid = $unitrow['id'];
        $user = Application::GetCurrentUser();
        $gameid = $game->ID;
        $userid = $user->ID;

        if (! ValidateMove($userid, $unitrow, $game, $x, $y)){
            die(json_encode( array( 'status' => 'Move validation failed') ));
            return; 
        }

        $query = "UPDATE units SET x=$x, y=$y, moves=0 WHERE id=$unitid";
        mysql_query($query) or die(json_encode( array( 'status' => mysql_error())));

        $query = "INSERT INTO gameupdates (gameid, type, param1, param2, param3) VALUES ($gameid, 1, $unitid, $x, $y)";
        mysql_query( $query ) or die( array( 'status' => mysql_error()) );

        $row=array('type' => 1, 'gameid' => $game->ID, 'id' => 1, 'param1' => $unitid, 'param2' => $x, 'param3' => $y); //NOTE THE UPDATE ID IS ALWAYS 1 HERE!!!!
        $updateList[] = GetUpdateObject($row);
        return; 
        
    }
    
    function attackApply($ability, $game, $unitrow, $newParams, &$updateList){
        $targx = $newParams['x'];
        $targy = $newParams['y'];
        $gameid = $game->ID;
        $targ = mysql_fetch_array(GetUnitByLoc($targx, $targy, $gameid));
        $user = Application::GetCurrentUser();
        $userid = $user->ID;
        $targid = $targ['id'];
        $unitid = $unitrow['id'];
        
        $s = GameDataService::GetInstance();
        $map = $s->GetGameMap( $game );
        
        if (! ValidateAttack($userid, $unitrow, $gameid, $targ)){
            die(json_encode( array( 'status' => 'Move validation failed') ));
            return;
        }
        
        $at = UnitConfiguration::GetByID( (int)$unitrow['type']  );
        $tt = UnitConfiguration::GetByID( (int)$targ['type']  );
        $t = $map->Cells[$unitrow['x']][$unitrow['y']]->Type;
        $tile = TileConfiguration::GetByID( $t );
        
        $defense = $tt->Defense;
        $attack = $at->AttackPower;
        
        $defense = $defense + $tile->DefenseBonus;

        DamageApply($attack, $defense, (int)$targ['hp'], $unitid, $targid, $targ['maxhp'], $gameid, $updateList);
    }
    
    function spawnunitApply($ability, $game, $unitrow, $newParams, &$updateList){
        $unitid = $unitrow['id'];
        $ownerid = $unitrow['ownerid'];
        $type = $newParams['param1'];
        $tarx = $newParams['x'];
        $tary = $newParams['y'];
        $user = Application::GetCurrentUser();
        $userid = $user->ID;
        $gameid = $game->ID;
        $t = UnitConfiguration::GetByID( $type );
        
        $moneyrow = mysql_fetch_array(GetMyMoney($userid, $gameid));
        $money = $moneyrow['resources'];
        
        if( $money < $t->ResourceCost)
        {
            echo json_encode( array( 'status' => "Not enough resources") );
            return;
        }
        //TODO: move this to validation and do more validation
        
        $money = $money - $t->ResourceCost;
        $query = "UPDATE usergames SET resources = $money WHERE gameid = $gameid AND userid = $userid";
        mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        
        $query = "UPDATE units SET actionused = TRUE WHERE id = $unitid";
        mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        
        $id = MySql::CreateUnit( $gameid, $ownerid, $tarx, $tary, $type );
        
        $query = "UPDATE units SET actionused = TRUE, moves = 0 WHERE id = $id";
        mysql_query( $query );
        
        $query = "INSERT INTO gameupdates (gameid, type, param1 ) VALUES ($gameid, 4, $id )";
        mysql_query($query) or die( array( 'status' => mysql_error()) );
        
        $row=array('type' => 4, 'gameid' => $gameid, 'id' => 1, 'param1' => $id); //NOTE THE UPDATE ID IS ALWAYS 1 HERE!!!!
        $updateList[] = GetUpdateObject($row);
    }
    
    //a few database utility functions - maybe these should go somewhere else like dbconn?

    function GetAllUnits($gameid){
        $query = "SELECT * FROM units WHERE gameid=$gameid";
        $result = mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        return $result;
    }
    
    function GetMyUnits($userid, $gameid, $dead = FALSE){
        if ($dead){
            $query = "SELECT * FROM units WHERE ownerid=$userid AND gameid=$gameid";
        } else {
            $query = "SELECT * FROM units WHERE ownerid=$userid AND gameid=$gameid AND hp > 0";
        }
        $result = mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        return $result;
    }
    
    function GetTheirUnits($userid, $gameid, $dead = FALSE){
        if ($dead){
            $query = "SELECT * FROM units WHERE ownerid != $userid AND gameid=$gameid";
        } else {
            $query = "SELECT * FROM units WHERE ownerid != $userid AND gameid=$gameid AND hp > 0";
        }
        $result = mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        return $result; 
    }
    
    function GetThisUnit($unitid){
        $query = "SELECT * FROM units WHERE id = $unitid";
        $result = mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        return $result;
    }
    
    function GetUnitByLoc($x, $y, $gameid){
        $query = "SELECT * FROM units WHERE x = $x AND y = $y AND gameid = $gameid";
        $result = mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        return $result;
    }
    
    function GetMyMoney($userid, $gameid){
        $query = "SELECT resources FROM usergames WHERE gameid = $gameid AND userid = $userid";
        $result = mysql_query( $query ) or die( array( 'status' => mysql_error()) );
        return $result;
    }
    
    
    //a few other utility functions
    function DamageApply($attack, $defense, $curhp, $unitid, $targid, $maxhp, $gameid, &$updateList){
        if ($curhp > 0){
            $damage = $attack-$defense;
            if ($attack > 0) {
                $damage = max($damage,0);
            }
            $newhp = max( min( $maxhp, $curhp - $damage ), 0 );

            $query = "UPDATE units AS Targ, units AS Att
                        SET Targ.hp = $newhp, Att.actionused = TRUE, Att.moves = 0
                        WHERE Att.id = $unitid AND Targ.id = $targid";
            mysql_query( $query ) or die( json_encode( array( 'status' => mysql_error()) ) );

            $query = "INSERT INTO gameupdates (gameid, type, param1, param2, param3) VALUES ($gameid, 2, $unitid, $targid, $damage)";
            mysql_query( $query ) or die( array( 'status' => mysql_error()) );

            $row=array('type' => 2, 'gameid' => $gameid, 'id' => 1, 'param1' => $unitid, 'param2' => $targid, 'param3' => $damage); //NOTE THE UPDATE ID IS ALWAYS 1 HERE!!!!
            $updateList[] = GetUpdateObject($row);
        }
    }
?>
