<?php

include('configuration.php');

class MySql
{
    public static function Init()
    {
        return MySql::openmysql();
    }
    
    public static function RegenerateTables()
    {
        MySql::drop_tables();
        MySql::create_tables();
        MySql::create_triggers();
        MySql::gen_default_data();
    }
    
    
    public static function CreateGame( $user1, $user2 )
    {
        return self::create_game( array( $user1, $user2));;
    }
    
    private static function CreateUser($name,$pass)
    {
        $name = mysql_real_escape_string( $name );
        $query = "INSERT INTO userpass (user, pass, color)
                    VALUES ('$name', '" . pass_hash($pass) . "', '#FFFFFF')";
        mysql_query($query) or die("mysql_error()");
    }
    
    // this may probly be moved to updatelib later maybe, or just be called idk
    
    public static function CreateUnit( $gameid, $oid, $x, $y, $type )
    {        
        return self::add_unit($gameid, $oid, $x, $y, $type);
    }
    
    //private
    
    private static $my_sql_link = null;

    private static function openmysql()
    {
        if( MySql::$my_sql_link ) return MySql::$my_sql_link;
    
        MySql::$my_sql_link = mysql_connect('localhost', 'derp', 'herpdong');

        if (!MySql::$my_sql_link) {
            die( mysql_error());
        }
        mysql_select_db("webgame") or die(mysql_error());
        
        return MySql::$my_sql_link;
    }
    
    private static function create_tables()
    {
        $query = 'CREATE TABLE userpass (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    user VARCHAR(200),
                    pass VARCHAR(60),
                    credits INT DEFAULT 0,
                    wins INT DEFAULT 0,
                    color VARCHAR(8))';

        mysql_query($query) or die(mysql_error());
        echo "<div>userpass table created</div>";

        $query = "CREATE TABLE games(
                    id INT NOT NULL AUTO_INCREMENT, 
                    PRIMARY KEY(id),
                    label VARCHAR(60),
                    activeuser INT NOT NULL,
                    updateid INT DEFAULT 0,
                    turn INT DEFAULT 1,
                    height INT, 
                    width INT,
                    seed INT,
                    INDEX( activeuser ),
                    FOREIGN KEY ( activeuser )
                        REFERENCES userpass(id))";      

        mysql_query($query) or die(mysql_error());
        echo "<div>games table created</div>";

        $query = "CREATE TABLE usergames(
                    gameid INT NOT NULL, 
                    userid INT NOT NULL,
                    resources INT NOT NULL DEFAULT 0,
                    PRIMARY KEY( gameid, userid ),
                    FOREIGN KEY( gameid )
                        REFERENCES games(id),
                    FOREIGN KEY( userid )
                        REFERENCES userpass(id))";      

        mysql_query($query) or die(mysql_error());
        echo "<div>usergames table created</div>";

        $query = "CREATE TABLE units(
                    id INT NOT NULL AUTO_INCREMENT, 
                    PRIMARY KEY(id),
                    name VARCHAR(30),
                    ownerid INT,
                    gameid INT NOT NULL,
                    type INT NOT NULL,
                    actionused BOOLEAN NOT NULL DEFAULT FALSE,
                    x INT NOT NULL, 
                    y INT NOT NULL,
                    hp INT NOT NULL,
                    maxhp INT NOT NULL,
                    moverange INT NOT NULL,
                    moves INT NOT NULL DEFAULT 0,
                    INDEX( gameid ),
                    FOREIGN KEY(gameid)
                        REFERENCES games( id ),
                    INDEX( ownerid ),
                    FOREIGN KEY(ownerid)
                        REFERENCES userpass( id ))";

        mysql_query($query) or die(mysql_error());
        echo "<div>units table created</div>";

        $query = "CREATE TABLE gameupdates(
                    id INT NOT NULL AUTO_INCREMENT,
                    PRIMARY KEY(id),
                    gameid INT NOT NULL,
                    type INT NOT NULL,
                    param1 INT NOT NULL,
                    param2 INT NOT NULL,
                    param3 INT NOT NULL,
                    param4 INT DEFAULT NULL,
                    FOREIGN KEY(gameid)
                        REFERENCES games( id ))";

        mysql_query($query) or die(mysql_error());
        echo "<div>gameupdates table created</div>";

        $query = "CREATE TABLE chatmessages(
                    id INT NOT NULL AUTO_INCREMENT,
                    PRIMARY KEY(id),
                    gameid INT NOT NULL,
                    username VARCHAR(30),
                    message VARCHAR(100),
                    FOREIGN KEY(gameid)
                        REFERENCES games( id ))";

        mysql_query($query) or die(mysql_error());
        echo "<div>chatmessages table created</div>";
    }

    private static function create_triggers()
    {
        $query = "CREATE 
                    TRIGGER gameupdatetrigger AFTER INSERT
                    ON gameupdates FOR EACH ROW BEGIN
                        UPDATE games SET games.updateid = NEW.id WHERE games.id = NEW.gameid;
                    END";
        mysql_query($query) or die(mysql_error());
        echo "<div>Trigger unitsgameupdate created</div>";
    }
    
    private static function get_game_active_player( $game_id )
    {
        $query = "SELECT user FROM userpass, games WHERE userpass.id = games.activeuser AND games.id = $game_id";
        $result = mysql_query($query);
        if( $row = mysql_fetch_array($result))
        {
            return $row['user'];
        }
        return null;
    }
        
    private static function add_unit( $gid, $oid, $x, $y, $type )
    {
        $name = GenerateName();
        if( $type == 0 )
            $name = GenerateBaseName();
        $t = UnitConfiguration::GetByID( $type );


        $query = "INSERT INTO units( gameid, ownerid, name, type, x, y, moves, moverange, hp, maxhp, actionused )
                    VALUES( $gid, $oid, '$name', $type, $x, $y, $t->MoveRange, $t->MoveRange, $t->MaxHP, $t->MaxHP, false )";
        mysql_query($query) or die(mysql_error()); 
        
        return mysql_insert_id();
    }

    private static function create_game( $users )
    {
        $r = rand();
        $name = NameGenerator::BattleNameGenerator();
        $name = $name->Generate();

        $query = "INSERT INTO games(label, activeuser, width, height, seed )
                    VALUES( '$name', 2, 21, 15, $r )";
        mysql_query($query) or die(mysql_error());



        $gameid = mysql_insert_id();
        echo "<div>game $gameid added</div>";

        $map = new Map( $gameid );

        $i = 0;
        $positions = array( array( 8,7, false), array(13,7, true), array( 20, 7 ));

        foreach( $users as $user )
        {

            $pos = array(rand(2,19), rand(2,14), false);
            $x = $pos[0];
            $y = $pos[1];
            $flip = $pos[2];

            $query = "INSERT INTO usergames( gameid, userid, resources )
                        VALUES( $gameid, $user, 2000 )";
            mysql_query($query) or die(mysql_error());


            self::add_unit($gameid, $user, $x, $y, 4);

        // add_unit($gameid, $user, $x+4, $y-4, 2);
        }    
        return $gameid;
    }

    private static function gen_default_data()
    {
        self::CreateUser('mikel3377','mikelfei');
        self::CreateUser('herp','dong');

        mysql_query("UPDATE userpass SET color='#FFFF00' WHERE user='herp'") or die(mysql_error());
        mysql_query("UPDATE userpass SET color='#0000FF' WHERE user='mikel3377'") or die(mysql_error());

        //for( $i = 0; $i < 5; $i++)
        //    gen_game(20+($i+1)*10, 1, 2);
        for( $i = 0; $i < 4; $i++)
            self::create_game( array( 1, 2));
        mysql_query("INSERT INTO chatmessages (gameid, username, message) VALUES (1, 'GOD', 'mike is a fag roflcopter')") or die(mysql_error());
    }

    private static function drop_tables()
    {
        mysql_query("DROP TABLE IF EXISTS units") or die(mysql_error());
        //echo "<div>units table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS unitgroups") or die(mysql_error());
        //echo "<div>unitgroups table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS gameupdates") or die(mysql_error());
        //echo "<div>gamemessages table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS chatmessages") or die(mysql_error());
        //echo "<div>chatmessages table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS gamemessages") or die(mysql_error());
        //echo "<div>gamemessages table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS usergames") or die(mysql_error());
        //echo "<div>usergames table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS games") or die(mysql_error()); 
        //echo "<div>games table deleted</div>";
        mysql_query("DROP TABLE IF EXISTS userpass") or die(mysql_error());
        //echo "<div>userpass table deleted</div>";
        echo "<div>all tables dropped</div>"; 
    }
}

function pass_hash($pass){
    //hashes a given password
    return hash_hmac('ripemd160', $pass, 'herpderp912');
}


?>
