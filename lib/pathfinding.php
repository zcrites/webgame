<?php

function AllCellsInRange($game, $x, $y, $range, $cost1=TRUE){
    $s = GameDataService::GetInstance();
    $map = $s->GetGameMap( $game );
    $cells=array();
    $utility=array();
    
    GetCellsInRange($map, $x, $y, $range, $cells, $utility, $cost1);
    return $cells;
}

class CandU{
    public $Cells;
    public $Utility;
    public function __construct(){
        $this->Utility = array();
        $this->Cells = array();
    }
}

function GetCellsInRange($map, $x, $y, $range, &$cells, &$utility, $cost1=TRUE){
    $continue = TRUE;
    if (! isset($utility[$x][$y])){
        $cells[$x][$y]=TRUE;
    } elseif ($range <=$utility[$x][$y]){
        $continue = FALSE;
    }

    if ($range > 0 && $continue){
        $utility[$x][$y]=$range;
        $neighbors=GetNeighbors($map, $x, $y);
        if ($cost1){
            for($i = 0; $i < sizeof($neighbors); ++$i){
                $nx = $neighbors[$i][0];
                $ny = $neighbors[$i][1];
                GetCellsInRange($map, $nx, $ny, $range-1, $cells, $utility, $cost1);
            }
        } else{
            for($i = 0; $i < sizeof($neighbors); ++$i){
                $nx = $neighbors[$i][0];
                $ny = $neighbors[$i][1];
                $t = $map->Cells[$nx][$ny]->Type;
                $tile = TileConfiguration::GetByID( $t );
                $newCost = $tile->Cost;
                if (! is_null($newCost)){
                    GetCellsInRange($map, $nx, $ny, $range-$newCost, $cells, $utility, $cost1);
                }
                
            }
        }
    }
}

function GetNeighbors($map, $x, $y){
    $neighbors=array();
    $xmin = ($x <= 0);
    $xmax = ($x >= $map->Width-1);
    $ymin = ($y <= 0);
    $ymax = ($y >= $map->Height-1);
    //$valid = ($x >= 0 && $y >= 0 && $x <= $map->Width-1 && $y <= $map->Height-1);
    //if ($valid){
        if (! $xmin){
            $neighbors[]=array($x-1,$y);
        }
        if (! $ymin){
            $neighbors[]=array($x,$y-1);
        }
        if (! $xmax){
            $neighbors[]=array($x+1,$y);
        }
        if (! $ymax){
            $neighbors[]=array($x,$y+1);
        }

        if ($x % 2 === 0){
            if (! ($xmax || $ymax)){
                $neighbors[]=array($x+1,$y+1);
            }
            if (! ($xmin || $ymax)){
                $neighbors[]=array($x-1,$y+1);
            }
        } else {
            if (! ($xmax || $ymin)){
                $neighbors[]=array($x+1,$y-1);
            }
            if (! ($xmin || $ymin)){
                $neighbors[]=array($x-1,$y-1);
            }
        }
        return $neighbors;
    //}
    //return 'INVALID INDICES';
    
}
?>
