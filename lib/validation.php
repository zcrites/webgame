<?php

    function ValidateMove($userid, $unitrow, $game, $x, $y){
        
        // TODO : Lots more validation for moves
        
        if( ! ValidateUnitOwner($userid, $unitrow))
        {
            die(json_encode( array( 'status' => 'That\'s not your unit!' ) ));
            return false; 
        }

        if( ! ValidateTurn($userid, $game->ID))
        {
            die(json_encode( array( 'status' => 'It\'s not your turn!') ));
            return false; 
        }

        if( ! ValidateUnitHasMoves($unitrow))
        {
            die(json_encode( array( 'status' => 'Unit has no moves.') ));
            return false;
        }
        
        return true;
    }
    
    function ValidateAttack($userid, $unitid, $gameid, $targ){
        
        //TODO: Lots more validation for attacks
        
        return true;
    }
    
    function ValidateUnitOwner($userid, $unitrow)
    {
        if ($unitrow['ownerid'] == $userid){
            return TRUE;
        }
        return FALSE;
    }
    
    function ValidateTurn($userid, $gameid)
    {
        $query = "SELECT activeuser FROM games WHERE id = $gameid";
        $result = mysql_query($query) or die( json_encode( array( 'status' =>mysql_error())));
        $row = mysql_fetch_row($result);
        if ($row && $userid == $row[0]){
            return TRUE;
        }
        return FALSE;
    }
    
    function ValidateUnitHasMoves($unitrow)
    {
        if ($unitrow['moves']>0){
            return TRUE;
        }
        return FALSE;
    }
?>
