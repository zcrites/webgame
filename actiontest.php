<?php
    include('lib/applicationlib.php');

    PageHelper::$PageTitle = "Action Test";
    
    PageHelper::AddScripts(array( 
        "scripts/actiontest.js"));
    
    $c = new MessageWidget();

        
PageHelper::Render( function () 
{
    global $c;
    ?>

<div class="centered-content">
    
    <div class="centered-form" >
        <button id="move-preset-button">Move</button>
        <button id="query-preset-button">Query</button>
        <button id="attack-preset-button">Attack</button>
    </div>
    
    <div class="centered-form" >
        <div>Page : <input id="page-target" type="text" value="action.php" /></div>
        <form id="action-form" action="action.php" method="POST">
            <div id="field-list" style="min-height:20em;">
            </div>
            <div style="clear:both;"></div>
        </form>
        <input id="submit-button"type="submit" />

    </div>
    <div class="centered-form">
        <input id="add-field-name" type="text" id="field-name" value="NEWFIELD"></input>
        <button id="add-field-button">Add Field</button>
    </div>
</div>

<div>
    <? $c->Render(); ?>
</div>



<?}); ?>