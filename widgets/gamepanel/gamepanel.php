<?php

    class GamePanelWidget extends Widget
    {        
        private $id;
        
        private $gamedata;
        private $first = true;
        
        public function RenderPanel( $label, $content)
        {
            $f = $this->first ? "ui-corner-tl" : ""; 
            ?>
            <div class="widget-portlet ui-widget ">
                <div class="ui-widget-header <?=$f?>">
                    <?=$label?>
                </div>
                <div class="ui-widget-content">
                    <? $content()?>
                </div>
            </div>
        <?
            $this->first = false;
        }
        
        public function GamePanelWidget()
        {
            //if( self::$new_id == 0 )    
            $this->addDefaultDependency("gamepanel");
            $this->id = self::$new_id++;
            
            $this->gamedata = Application::$GameData;
            
        }
        
        public function Render()
        {
?>
<div class="GamePanelWidget ui-widget" ng-controller="GameDataCtrl">
    <div class="gamepanel-bg ui-widget-content ui-corner-left content-fill-to-margin">
        
    </div>
    <div class="panels-top" style="height:80%">
        
    <? self::RenderPanel( $this->gamedata->Label, function()        
    {?>
        <div>
            It's {{gameData.ActivePlayer.UserName}}'s turn.
        </div> 

    <?}) ?>
    
    <? self::RenderPanel( "{{ gameData.ActivePlayer.UserName }}", function()        
    {?>
                 <div>Resources : {{gameData.ActivePlayer.Resources}}</div>
                 <div>
                     <div ng-repeat="unit in gameData.Units" class="unit-list-div {{ unit.GetCssClass() }}" ng-show="unit.OwnerID == gameData.ActivePlayer.UserID">
                         <div>
                             
                         {{unit.Name}} - {{unit.TypeName}}
                                                 </div>
                     </div>
                 </div>
    <?}) ?>
    </div>
    <div class="panels-bottom" ng-controller="CanvasMouseOverCtrl">
        <? self::RenderPanel( "{{ CellTypeName() }}", function()        
    {?>
        <div>
            It's {{gameData.ActivePlayer.UserName}}'s turn.
        </div> 

    <?}) ?>
    </div>
                 

</div>

<?
        }
    }
    
?>