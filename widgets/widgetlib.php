<?php
    include_once('lib/dbconns.php');
    
    abstract class Widget
    {
        protected static $new_id = 0;

        
        static function RenderPortlet( $label, $content )
        {
            ?>
    <div class="widget-portlet ui-widget">
        <div class="ui-widget-header ui-corner-top">
            <?=$label?>
        </div>
        <div class="ui-widget-content ui-corner-bottom">
            <? $content()?>
        </div>
    </div>
            <?
        }
        
        public abstract function Render();
        
        //private function addWidgetDeps( "")
        
        protected function addDefaultDependency( $name )
        {
            PageHelper::AddScript( "widgets/$name/$name.js");
            PageHelper::AddStyle( "widgets/$name/$name.css");
        }
    }
    
    include( 'gamelist/gamelist.php');
    include( 'messagewidget/messagewidget.php');
    include( 'gamepanel/gamepanel.php');
    
?>
