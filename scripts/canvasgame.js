 

function ContextInfoCtrl( $scope, $rootScope, dataService )
{    
    $scope.units = $scope.gameData.Units;//$rootScope.gameData.Units;
    
}

function GameDataCtrl( $scope, dataService )
{
    $scope.gameData = dataService.GetGameData();
    
    WebGame.Unit.prototype.GetCssClass = function() 
    { 
        var s = "";
        if( (this.Moves < 1 && !this.Moving) || this.HP <= 0 || this.OwnerID != $scope.gameData.ActiveUser )
            s += " item-disabled"; 
        return s;
    };
}

function CanvasMouseOverCtrl( $scope, dataService )
{
    var data = dataService.GetGameData();
    $scope.displayMode = "none";
    $scope.cellDisplay = false;
    $scope.cell = null;
    $scope.cellType = null;
    $scope.unit = null;
    
    
    $scope.CellTypeName = function()
    {
        return ( $scope.cell == null ) ? '0' : TileConfig[$scope.cell.Type].Name;    
    }
    
    $scope.CellMoveCost = function()
    {
        return ( $scope.cell == null ) ? '0' : TileConfig[$scope.cell.Type].Cost;    
    }
    
    data.Map.applyToAllCells( function()
    {
        var cell = this;
        this.onMouseover = function(x, y)
        {
            WebGame.Cell.prototype.onMouseover.call(this, x, y );

            $scope.$apply(function(){
                $scope.displayMode = "cell";
                $scope.cell = cell;
                $scope.cellType = TileConfig[$scope.cell.Type];
                if( cell.Unit != null )
                {
                    $scope.unitImage = UnitConfig[cell.Unit.Type].ImageSrc;
                    $scope.displayMode = "unit";
                    $scope.unit = cell.Unit;
                    if( cell.Unit.TypeName === "Fortress")
                        $scope.displayMode = "fortress";
                }
            });
        }
    });
    
}

$(document).ready(function(){
    
    
    //var Herp = new WebGame.GameData( GameData );

    var $injector = angular.injector(['ng']);
    var dataService = $injector.get('dataService');
    var updateManager = $injector.get('updateManager');
    //var animationManager = $injector.get('animationManager');
    
    var Herp = dataService.GetGameData();     
    //dataService.EnableGameDataUpdates();
    
    
    //dataService.AddWatch( 'gameData.ActiveUser', function() { alert('der')})
    
    var canvas = document.getElementById("map-canvas")
    Herp.InitializeCanvas(canvas, ImagesLoaded );
    Herp.CanvasEventsInit(canvas);
    Herp.CanvasAnimationsInit(canvas);
    
    function ImagesLoaded()
    {
        //SetClickFn( DefaultClick );
        render();
        //Herp.EnableUpdates( update, true );
    }

    var update = function ()
    {
        //Herp.render(0,0);
    };
    
    var render = function()
    {
        setTimeout(render, 16);
        Herp.render(0,0);
        Herp.Map.animationManager.drawAll();
    }
    
    
    if( UserData.ID != Herp.ActiveUser )
    {
        updateManager.Enable();
    }
    
    $("#end-turn-button").click(function(){
       $.ajax({
            type : "POST",
            url : "action.php",
            dataType : "json",
            data : {action: 'endturn', gameid : Herp.ID },
            success : function ( data ) {
                console.log(data);

                dataService.UpdateGameData(function(){
                    updateManager.Enable();
                });
            },
            error : function ( data )
            {      
                WebGame.Console.printObject( data );

                dataService.UpdateGameData(function(){
                    updateManager.Enable();
                });                //updateManager.StartUpdates();
            }
        });   
    });
    
});
   