/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ( window ){
    
    function UpdateManager( dataService )
    {
        var gameData = dataService.GetGameData();
        var enabled = false;
        var updateID = gameData.UpdateID;
        var manager = this;
        this.Updates = [];
        
        function Update()
        {

            if( manager.Updates.length == 0 )
            {
                $.ajax({
                    type : "POST",
                    url : "query.php",
                    dataType : "json",
                    data : { gameid : gameData.ID, updateid : updateID, updates : true },
                    success : function ( response ) {
                        manager.Updates = manager.Updates.concat( response.updates );
                        for( var i = 0; i < response.updates.length; i++ )
                        {
                            //WebGame.Console.printObject( response.updates[i] );
                            //WebGame.Console.printObject( response.updates[i].UpdateID );
                            updateID = response.updates[i].UpdateID;
                        }
                        if( response.updates.length == 0 )
                            setTimeout( Update, 500 );                    
                        else
                            Update();
                    },
                    error : function ( response )
                    {
                        WebGame.Console.writeln( "error ( id : "+gameData.ID+" )");
                    }
                });
            }
            else
            {
                var queue = manager.Updates;
                manager.Updates = [];

               // WebGame.Console.writeln( " updating ( gameid : "+gameData.ID+", updateid : "+gameData.UpdateID+" )");

                //pop update, handle animation/ call Update to continue when finished

                //or just ignore them all and update for now ( turn off auto updates if it's back to your turn though )
                for( var i = 0; i < queue.length; i++ )
                {
                    var update = queue[i]; 
                    WebGame.Console.writeln( "processing update ( id : "+update.UpdateID+", type : "+update.TypeName+" )");

                    if( update.TypeName === "BeginTurn" )
                    {
                        //WebGame.Console.printObject( { newid : update.NewPlayerID, current : UserData.UserID } );
                        if( update.NewPlayerID === UserData.ID )
                        {
                            //WebGame.Console.writeln( gameData.ActiveUser+"'s turn, pausing auto updates...");
                            dataService.UpdateGameData();
                            enabled = false;
                            WebGame.Console.writeln( gameData.ActiveUser+"'s turn, pausing auto updates...");
                            return;                                
                        }
                    }
                }

                dataService.UpdateGameData();
                Update();
            }
            
        }
        
        this.Enable = function()
        {
            updateID = gameData.UpdateID;
            manager.Updates = [];
            if( !enabled )
            {
                WebGame.Console.writeln( gameData.ActiveUserName+"'s turn, enabling auto updates...");
                Update();
            }
            enabled = true;
        }
        
       /* this.Disable = function()
        {
            this.Updates = [];
            enabled = false;
        }*/
        
        this.PopUpdate = function () {
            if( manager.Updates.length == 0)
                return null;
            return manager.Updates.shift();
        }
        
    }
    
    var webGame = angular.module('ng');
    var updateService = null;
    
    webGame.factory( 'updateManager',  function( dataService ){
        if(updateService === null )
        {
            updateService = new UpdateManager( dataService ); 
        }
       return updateService; 
    });
    
    
})( window );

