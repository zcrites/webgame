/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

ToolTip = null;

function ToolTipCtrl( $scope, $rootScope )
{   
    $scope.display = 'block';
    $scope.left = '0';
    $scope.top = '0';
    
    var cancel = function() { };
    var intercept = false;
    var element = $("#tooltip-control");

    function ToolTipItem( name, handler )
    {
        this.label = name;
        this.click = function($event)
        {
            //WebGame.Console.printObject( $event, false );
            $event.stopPropagation();
            handler();
            element.css( 'display', 'none');
        }
    }
    
    
    $scope.$watch( function()
    {
        element.find("div").button();
    });

    $rootScope.rootClick = function( $event ) { 
        //WebGame.Console.writeln( "click : "+$event.pageX +", "+$event.pageY);
        element.css( 'left', $event.pageX );
        element.css( 'top', $event.pageY );       
        if( element.css('display') != 'none' && !intercept)
        {
            element.css('display', 'none');
            cancel();
        }
        intercept = false;
    };
    
    $scope.items = [];
        
    this.Show = function()
    {
        intercept = true;
        element.css('display', 'block');
        //WebGame.Console.writeln("ShowTooltip");
    }
    
    this.Clear = function()
    {
        element.css("display","none");
        cancel = function() { }
        $scope.items = [];
        return this;
    }
    
    this.AddButton = function( label, handler )
    {
        $scope.items.push( new ToolTipItem( label, handler ));
        return this;
    }
    
    this.PushButton = function( label, handler )
    {
        $scope.items.push( new ToolTipItem( label, handler ));
        return this;
    }
    
    this.Size = function() {return $scope.items.length; };
    
    this.SetCancel = function( handler, showbutton )
    {
        if( typeof showbutton === 'undefined') showbutton = false;
        cancel = handler;
        if( showbutton )
            $scope.items.push( new ToolTipItem( "Cancel", handler ));
        return this;
    }
        
    ToolTip = this;
}

