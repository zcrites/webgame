

(function ( window )
{   
    // -- Initialize base object constructors and methods --
    function ServerDataObject() {}    
    ServerDataObject.prototype.Update = function( objectData )
    {
        var updated = false;
        for( var field in objectData )
        {
            if( typeof objectData[field] !== 'function' &&
                typeof objectData[field] !== 'object' &&
                this[field] !== objectData[field])
            {        
                this[field] = objectData[field];
                updated = true;                
            }
        }        
        return updated;        
    };
    
    function Unit( unitData )
    {
        this.Update( unitData );
        var dataService = angular.injector( ['ng']).get( 'dataService' );
        
        this.AddWatch = function( expr, fn )
        {
            var wrapper = function( newV, oldV ){
                if( oldV != newV )
                    fn( newV, oldV );
            }
            dataService.AddWatch( 'gameData.Units['+this.ID+'].'+expr, wrapper );
        };
        
        var unit = this;
        this.AddWatch( 'HP', function(newV, oldV) {
            WebGame.Console.writeln(unit.OwnerName+"'s "+unit.TypeName +" takes "+(oldV-newV)+" damage.");
        });
    }
    
    Unit.prototype = new ServerDataObject();
    
    Unit.prototype.GetHP = function()
    {
        return (this.HP > 0 ) ? this.HP : 0;
    };

    function Cell( cellData )
    {
        ServerDataObject.prototype.Update.call(this, cellData );
                
        var neighbors = [];
        this.addNeighbor = function( n ) { neighbors.push( n ); }
        this.getNeighbors = function() { return neighbors; }
    }
    Cell.prototype = new ServerDataObject();

    function Map( mapData )
    { 
        this.Update( mapData );
    }
    Map.prototype = new ServerDataObject();
    
    Map.prototype.Update = function( mapData )
    {

        var currentMap = this;
        ServerDataObject.prototype.Update.call(this, mapData );
        this.Cells = [];
        for( var i = 0; i < this.Width; i++ )
        {
            this.Cells[i] = [];
            for( var j = 0; j < this.Height; j++ )
            {
                this.Cells[i][j] = new Cell( mapData.Cells[i][j]);    
                this.Cells[i][j].GetMap = function() { return currentMap; }
                this.Cells[i][j].X = i; //Mike added these
                this.Cells[i][j].Y = j;
            }
        }
        
        function makeNeighbors( cell1, cell2 )
        {
            cell1.addNeighbor( cell2 );                       
            cell2.addNeighbor( cell1 );
        }

        //link neighbors
        for( i = 0; i < this.Width; i++ )
        {
            for( j = 0; j < this.Height; j++ )
            {
                if( j < this.Height - 1 )
                    makeNeighbors(this.Cells[i][j],this.Cells[i][j+1] );            
                if( i < this.Width - 1 )
                    makeNeighbors(this.Cells[i][j],this.Cells[i+1][j] );

                if( i % 2 == 0)
                {
                    if( i < this.Width - 1 && j < this.Height - 1 ) 
                        makeNeighbors(this.Cells[i][j],this.Cells[i+1][j+1] );
                }
                else
                {
                    if( i < this.Width - 1 && j > 0 )
                        makeNeighbors(this.Cells[i][j],this.Cells[i+1][j-1] );         
                }
            }
        }

        return true;
    };
    
    Map.prototype.applyToAllCells = function( fn )
    {
        for( var i = 0; i < this.Width; i++ )
        {
            for( var j = 0; j < this.Height; j++ )
            {
                    fn.call( this.Cells[i][j], i, j )
            }
        }        
    };
    
    function GameData( gameData )
    {
        this.Units = {};
        this.Players = {};
        this.Update( gameData );        
    }
    GameData.prototype = new ServerDataObject();
    
    GameData.prototype.GetActivePlayer = function()
    {
        for( var i = 0; i < this.Players.length; i++)
        {
            if( this.Players[i].UserID == UserData.ID)
                return this.Players[i];
        }
        return null;
    }
    
    GameData.prototype.Update = function ( gameData )
    {
        var updated = ServerDataObject.prototype.Update.call(this, gameData );
        var i, j;

        this.ActivePlayer = gameData.ActivePlayer;

        for( i in gameData.Units )        
        {
            if( typeof this.Units[ gameData.Units[i].ID ] === 'undefined')
            {
                this.Units[ gameData.Units[i].ID ] = new Unit( gameData.Units[i] );
                this.Units[ gameData.Units[i].ID ].Game = this;
                updated = true;
            }
            else
            {
                if( this.Units[gameData.Units[i].ID].Update( gameData.Units[i] ))
                    updated = true;
            }
        }
        
        if( typeof (gameData.Map) !== 'undefined' )
        {
            this.Map = new Map( gameData.Map );  
        }
        
        if( typeof (gameData.Players) !== 'undefined' )
        {
            this.Players = gameData.Players;
        }
                
        return updated; 
    };
    
    var lib = new (function()
    {
        this.Cell = Cell;
        this.Unit = Unit;
        this.Map = Map;
        this.GameData = GameData;           

//        this.TileConfig =
        
        //this.TerrainNames = [ "Grass", "Tall Grass", "Linoleum"];
    })();

    window.WebGame = lib;    
    
    // -- Initialize angular services --
    
    function Animation()
    { 
        this.Finised = false;
    }
    Animation.prototype.update = function() { };
    Animation.prototype.onFinished = function() { };
                
    function AnimationManager()
    {
        var manager = this;
        var animations = [];
        //var render = [];

        this.NewAnimation = function()
        {
            var anim = new Animation();            
            animations.push(anim);
            return anim;
        }

        /*this.DelayAnimation = function( duration, finFn )
        {
            var t = duration;
            var anim = new Animation();
            anim.update = function(){ anim.Finished = (t-- <= 0);}
            anim.onFinished = function() { 
                 WebGame.Console.writeln("finished!");

            };
            if( typeof finFn !== 'undefined')
                anim.onFinished = finFn;
            animations.push( anim );
        }
        
        this.AttackAnimation = function( attacker, defender )
        {
            WebGame.Console.writeln("Attacking...");
            manager.DelayAnimation( 1000 );          
        }       */ 
                
        function Update()
        {
            setTimeout(Update, 16);
            var i;

           // WebGame.Console.writeln("render");

            for( i = 0; i < animations.length; i++ )
                animations[i].update();
            for( i = 0; i < animations.length; i++ )
            {
                if( animations[i].Finished )
                {
                    animations[i].onFinished();
                    animations.splice(i--,1);
                }
            }
        }
        
        Update();
        
    }
    
    var webGame = angular.module('ng');
    var animationManager = null;
    
    webGame.factory( 'animationManager',  function( updateManager ){
        if(animationManager === null )
        {
            animationManager = new AnimationManager( updateManager ); 
        }
       return animationManager; 
    });
    
    
    var dataservice = null;
    
    webGame.factory( 'dataService',  function( $rootScope ){
        if(dataservice === null )
        {
            dataservice = new (function (){
                var service = this;
                var updatesOn = false;
                
                this.InitializeGameData = function( gameData )
                {
                    $rootScope.gameData = new GameData( gameData );

                    $rootScope.gameData.AddWatch = function( expr, fn )
                    {
                        var wrapper = function( newV, oldV )
                        {
                            if( newV != oldV ) fn( newV, oldV );
                        }
                        $rootScope.$watch( 'gameData.'+expr, wrapper );
                    }
                };
                
                this.GetGameData = function()
                {
                    return $rootScope.gameData;
                };
                
                this.EnableGameDataUpdates = function( callback )
                {
                    updatesOn = true;
                    QueueUpdate();
                    
                    function QueueUpdate()
                    {
                        if( !updatesOn ) return;
                        service.UpdateGameData( callback );
                        setTimeout( QueueUpdate, 2000 );
                    }
                };
                
                this.DisableGameDataUpdates = function()
                {
                    updatesOn = false;
                };

                
                this.AddWatch = function( expr, fn )
                {
                    $rootScope.$watch( expr, fn );
                }
                
                this.UpdateGameData = function( callback )
                {
                    var data = $rootScope.gameData;
                    if( typeof callback === 'undefined' ) callback = function() { };
                    $.ajax({
                        type : "POST",
                        url : "query.php",
                        dataType : "json",
                        data : { gameid : data.ID, updateid : data.UpdateID },
                        success : function ( response ) {
                            //WebGame.Console.writeln( data.UpdateID );
                            //WebGame.Console.printObject(response); 
                            

                            if( response.status == 'UPDATE')
                            {    

                                if( data.UpdateID < response.data.UpdateID )
                                {
                                    var updated = false;
                                    $rootScope.$apply( function()
                                    {
                                        updated = data.Update( response.data );
                                    });
                                    //if( updated )
                                    //return;
                                }
                            }
                            if( response.status == 'NO UPDATE')
                            {    
                                //if(!silent)WebGame.Console.writeln( "No update data!");               
                            }
                            else
                            {   
                               // WebGame.Console.printObject( response, false );                        
                            }
                            callback();

                        },
                        error : function( response )
                        {
                        }
                    });
                }
            })();
            dataservice.InitializeGameData( Data );
        }
       return dataservice; 
    });
    
    
})( window );

function UpdateData( gameData )
{
    return;
    var $injector = angular.injector(['ng']);
    
    //WebGame.Console.writeln( "udating game:" );
    $injector.invoke( function( )
    {
        return;
        
        //WebGame.Console.printObject( $http, false )
        /*
        $.ajax({
            type : "POST",
            url : "query.php",
            dataType : "json",
            data : { gameid : Data.ID, updateid : Data.UpdateID },
            success : function ( response ) {
                if( response.status == 'UPDATE')
                {    
                    if( Data.UpdateID < response.data.UpdateID && Data.Update( response.data ) )
                    {
                        if(!silent)WebGame.Console.writeln( "GameData updated!");  
                        callback();
                        return;
                    }
                }
                if( response.status == 'NO UPDATE')
                {    
                    if(!silent)WebGame.Console.writeln( "No update data!");               
                }
                else
                {   
                    WebGame.Console.printObject( response, false );                        
                }
            } 
        });*/
    });
    
};


$(document).ready(function(){
    
    //-- CONSOLE / LOGGING ------------------
    function Console()
    {
        this.showObjectFunctions = false;
        this.logLimit = 999999;
        this.line = 0;
        
        
        this.element = $("#console-display");
        if( this.element.size() == 0 )
            this.element = $(".console-display");
        
        this.writeln = function( s )
        {
            this.setEnabled( true );
            if( this.line > this.logLimit )
            {
                this.element.children().first().remove();
            }
            this.element.append( "<div><div class='console-label'>"+ (this.line++) +":</div><div> "+s+"</div></div>");
            this.element.scrollTop( this.logLimit * 20 );
            this.element.parent().scrollTop( this.logLimit * 20 );
        }
        
        this.setEnabled = function( enabled )
        {
            //if( enabled )
            ///    this.element.parent().css( "display", "block");
            //else
           //     this.element.parent().css( "display", "none");
        }
        
        this.printObject = function ( obj, recursive, tabs )
        {
            if( typeof recursive === 'undefined' ) recursive = false;
            if( tabs === undefined ) tabs = "";
            for( var i in obj )
            {
                if (Object.prototype.toString.call(obj) === '[object Array]' || obj instanceof Array){
                    if (recursive){
                        this.writeln(tabs + 'Array index ' + i);
                        this.printObject( obj[i], true, tabs + "   ");
                    } else{
                        this.writeln( tabs + i + " : (Array)");
                    }
                }
                else if( typeof obj[i] === 'object')
                {
                    if( recursive )
                    {                      
                        this.writeln(tabs + 'Object index ' + i);
                        this.printObject( obj[i], true, tabs + "   ");
                    } else {
                        this.writeln( tabs + i + " : (Object)");
                    }
                }
                else if( typeof obj[i] === 'function' )
                {
                    if( this.showObjectFunctions )
                        this.writeln(tabs + i + " : (function)");
                }
                else
                {
                    this.writeln( tabs + i + " : " + obj[i]+"   ("+(typeof obj[i])+")");
                }
            }
        }
    }   
    
    //-- GAME OBJECT SYNCHRONIZATION 
    
    
    (function ( window )
    {
       
        window.WebGame.Console = new Console();// = lib;
        
        $('button').button();
        $('input[type=submit]').button();
        $('input[type=submit]').button();
        
        $('.link-div a').button();

        //$('a').button();   
        
        WebGame.Console.writeln("Core library loaded!");
    })(window);     
    
});


