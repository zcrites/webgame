(function(window){
    
    
})(window);

$(document).ready(function(){
    WebGame.Console.writeln("Canvas animations library loaded!");
    var Console = WebGame.Console;
    var canvas, context;
    var Data;
    
    WebGame.GameData.prototype.CanvasAnimationsInit=function(canvaselement, onLoaded){
        WebGame.Console.writeln("Initializaing canvas animations...");
        canvas = canvaselement;
        context = canvas.getContext('2d');
        Data = this;
        Data.Map.animationManager = new AnimationManager();
    }
    
    function AnimationManager(){
        this.animations=[];
        this.numAnimations=0;
        
        this.addAttackAnimation = function(Attacker,Target,damage){
            var TCoords=Data.Map.visSettings.tileToGameCoords(Target.X+0.5,Target.Y+0.5);
            var ACoords=Data.Map.visSettings.tileToGameCoords(Attacker.X+0.5,Attacker.Y+0.5);
            var newAnimation = new MovingObjectAnimation(ACoords[0],ACoords[1],TCoords[0],TCoords[1],1);
            var font = "bold 22px arial";
            var damage = Math.max(damage,0);
            newAnimation.nextAnimation = new FloatingMessageAnimation(TCoords[0]-10,TCoords[1]+0,String(-damage),font,'red',119);
            
            this.animations[this.numAnimations] = newAnimation;
            
            this.numAnimations +=1;
        }
        
        this.drawAll = function(){
            //WebGame.Console.writeln('drawing all')
            var i =0;
            while(i < this.animations.length){
                //WebGame.Console.writeln(['drawing',i])
                var animation=this.animations[i];
                animation.draw();
                animation.time += 1;
                if (animation.time > animation.life) {
                    if (animation.nextAnimation == null){
                        this.animations.splice(i,1)
                        i += -1;
                        this.numAnimations -=1;
                    } else {
                        this.animations[i]=animation.nextAnimation;
                    }
                } else {
                    animation.changeIfNeeded();
                }
                i += 1;
            }
        }
        
    }
    
    function AnimationObject(){
        this.nextAnimation=null;
        this.time=0;
        this.changeIfNeeded=function(){
            if (this.time % this.changeInterval == 0){
                this.change();
            }
        }
    }
    
    function MovingObjectAnimation(x1,y1,x2,y2,speed){
        this.life = Math.sqrt(Math.pow(y2-y1,2) + Math.pow(x2-x1,2))/speed;
        //var angle = Math.atan2((y2-y1),(x2-x1));
        //var speedX = Math.cos(angle);
        //var speedY = Math.sin(angle);
        this.changeInterval=Math.max(Math.floor(1/speed),1);
        //WebGame.Console.writeln(this.changeInterval);
        this.dx=(x2-x1)/this.life*this.changeInterval;
        this.dy=(y2-y1)/this.life*this.changeInterval;
        this.curX=x1;
        this.curY=y1;
        
        this.draw=function(){
            context.fillStyle='red';
            context.fillRect(this.curX,this.curY,5,5);
        }
        
        this.change = function(){
            this.curX += this.dx;
            this.curY += this.dy;
        }
    }
    
    MovingObjectAnimation.prototype = new AnimationObject();
    
    function FloatingMessageAnimation(x,y,message,font,color,life){
        this.life=life;
        this.changeInterval=2;
        this.x=x;
        this.y=y;
        this.font=font;
        this.message=message;
        this.color=color;
        
        this.draw=function(){
            //WebGame.Console.writeln('drawing');
            context.font = this.font;
            context.fillStyle=this.color;
            context.fillText(this.message,this.x,this.y);
        }
        
        this.change = function(){
            this.y += -0.25;
        }
    }
    
    FloatingMessageAnimation.prototype = new AnimationObject();
});
