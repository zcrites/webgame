<?php
    include 'lib/applicationlib.php';
    
    if( isset($_POST['realname']) && isset($_POST['mypassword']) && isset($_POST['mypassword2']) )
    {
        $username=$_POST["realname"];
        $pass1=$_POST['mypassword'];
        $pass2=$_POST['mypassword2'];
        if ($pass1 == $pass2){
            if (strlen($pass1) >2){ //password requirement logic here

                if (!Application::GetUserByName($username)){

                    Application::CreateUser($username, $pass1);
                    PageHelper::Redirect( "login.php");
                }
                else{ //user already exists

                    PageHelper::Redirect( "register.php");
                }
            }
            else{ //password doesn't meet requirements
                PageHelper::Redirect( "register.php");
            }
        }
        else { 
            PageHelper::Redirect( "register.php");
        }
        return;
    }
    
    PageHelper::$PageTitle = "New user registration";
    PageHelper::AddScript( "scripts/index.js");
    PageHelper::$RequireLogin = false;
    PageHelper::Render( function() {
        
        
if (isset($_GET['error']) AND !empty($_GET['error']))
{
    $errornum=$_GET['error'];
    if ($errornum==1){
        print 'Passwords did not match. Please try again.';
    }
    elseif ($errornum==2){
        print 'Your password was too short. Please try again.';
    }
    elseif ($errornum==3){
        print 'That user already exists. Please try a different username.';
    }
    
}
?>
<FORM ACTION=register.php METHOD=POST>
    username: <INPUT TYPE=TEXT NAME="realname"><BR>
    password: <INPUT TYPE=PASSWORD NAME="mypassword"><BR>
            password: <INPUT TYPE=PASSWORD NAME="mypassword2">
    <P><INPUT TYPE=SUBMIT VALUE="submit">
</FORM>

<BR><BR><BR>

<FORM ACTION=index.php METHOD=POST>
    <P><INPUT TYPE=SUBMIT VALUE="Back to login page">
</FORM>
  

   
  <?      
    });
?>