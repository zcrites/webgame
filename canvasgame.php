<?php

    include 'lib/applicationlib.php';
    PageHelper::AddStyle("styles/canvasgame.css");
    PageHelper::AddStyle("styles/tooltip.css");
    
    PageHelper::$PageTitle = "Strategy Fight Tycoon";
    PageHelper::AddScripts(array( 
        "scripts/updatemanager.js",
        "scripts/tooltipctrl.js",
        "scripts/pathfinding.js",
        "scripts/canvasrendering.js",
        "scripts/canvasevents.js",
        "scripts/cellHandlers.js",
        "scripts/canvasanimations.js",
        "scripts/canvasgame.js"));
    
            
    $c = new MessageWidget();
    $panel = new GamePanelWidget();

    PageHelper::$GETGameData = TRUE;
    PageHelper::Render( function() {
        global $c;
        global $panel;
?>
<div>
    <div id="tooltip-control" class="ui-corner-all ui-widget-content" ng-controller="ToolTipCtrl"> 
    
        <div ng-repeat="item in items" ng-click="item.click($event)">
            {{ item.label }}
        </div>
    </div>
    <div>
    </div>
    <div id="map-container">
    
        <div  class ="left-game-panel">
            
            <?$panel->Render(); ?>

        </div>

        <canvas id="map-canvas" width="1000" height="618px">        
        </canvas>

    </div>
    <div>
        
    </div>
    
    <div style="clear:both;"><button id="end-turn-button" >End Turn</button></div>
    
</div>

<div style="margin-top:.5em;">
        <?
            $c->Render();
        ?>
</div>

<?php });?>
