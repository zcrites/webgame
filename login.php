<?php
    include 'lib/applicationlib.php';
    
    
    if( isset( $_POST['username']) && isset($_POST['password']) )
    {
        MySql::Init();

        $username = $_POST['username'];    // Gets the inputted username from the form
        $password = $_POST['password'];    // Gets the inputted password from the form
        $time = time();            // Gets the current server time
        if (isset($_POST['setcookie'])){
            $check = $_POST['setcookie'];        // Checks if the remember me button was ticked
        }
        else{
            $check = FALSE;
        }
        if( Application::GetUserByName($username) && Application::CheckPassword($username, $password)) {    // If the username and password are correct do the following;
            $_SESSION['username'] = $username;
            if($check) {
                // Check to see if the 'setcookie' box was ticked to remember the user
                setcookie("zacmikewebgame[username]", $username, $time + 3600);        // Sets the cookie username
                setcookie("zacmikewebgame[password]", $password, $time + 3600);    // Sets the cookie password
            }
        }
        else    // If login is unsuccessful forwards the user back to the index page with an error
        {
            PageHelper::Redirect("login.php");

        }
        PageHelper::Redirect("index.php");
        return;
    }
    
    PageHelper::$PageTitle = 'Log In';
    
    PageHelper::AddStyle( "styles/index.css");
    PageHelper::AddScript( "scripts/index.js");
    PageHelper::$RequireLogin = false;
    
    PageHelper::DisableConsole();
    
    session_unset();
    session_destroy();
    

    
    if(isset($_COOKIE['zacmikewebgame'])) {
        // If the cookie 'Joe2Torials is set, do the following;

        $username = $_COOKIE['zacmikewebgame']['username'];
        $password = $_COOKIE['zacmikewebgame']['password'];

        if(user_exists($username) && check_pass($username, $password)) {
            // If the login information is correct do the following;
            $_SESSION['username'] = $username;
            header('Location: game.php');
        }
    }

    if (isset($_GET['error']) AND !empty($_GET['error']))
    {
        $errornum=$_GET['error'];
        if ($errornum==1){
            PageHelper::RenderError( 'Invalid login data supplied. Please try again.');
        }
        else if ($errornum==2){
            PageHelper::RenderError( 'User account successfully created!');
        }
        return;
    }

    PageHelper::Render( function () 
    {
?>

<div class="centered-form">
    
<form method="post" name="cookie" action="login.php">
    <!--<p><label> Log in here </label></p>-->
    <p><label for="username">Username : <input type="text" name="username" id="username" /></label></p>
    <p><label for="password">Password : <input type="password" name="password" id="password" /></label></p>
    <p><input type="checkbox" name="setcookie"/> Remember Me</p>
    <p><input type="submit" name="submit" value="Submit" /> <input type="reset" name="reset" value="Reset" /></p>
</form>
</div>

<div class="centered-form">

   <a href="register.php">Create New Account</a>
        
</div>
<?php 
});
